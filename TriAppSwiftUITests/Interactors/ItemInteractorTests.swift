//
//  ItemInteractorTests.swift
//  Tests iOS
//
//  Created by Steven Hendrie on 19/02/2021.
//

import XCTest
import SwiftUI

@testable import TriAppSwiftUI

class ItemInteractorTests: XCTestCase {

    var shopItemInterator: ShopItemInteractor!
    
    var mockRepository: MockItemRepository!
    var appState: AppState!
    
    override func setUpWithError() throws {
        
        setupInteractorWithError(error: false)
    }
    
    private func setupInteractorWithError(error: Bool) {
        self.mockRepository = MockItemRepository()
        self.mockRepository.loadError = error
        self.appState = AppState()
        
        self.shopItemInterator = ShopItemInteractor(itemRepository: mockRepository, appState: appState)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLoadItem() throws {
        XCTAssertTrue(appState.staticData.items.isEmpty)
        
        self.shopItemInterator.loadItem()
        
        XCTAssertFalse(appState.staticData.items.isEmpty)
        XCTAssertEqual(appState.staticData.items.first!, Item.preview)
        
    }
    
    func testLoadItemFail() throws {
        
        setupInteractorWithError(error: true)
        XCTAssertTrue(appState.staticData.items.isEmpty)
        
        self.shopItemInterator.loadItem()
        
        XCTAssertTrue(appState.staticData.items.isEmpty)
    }
    
}

struct MockItemRepository: ItemRepository {
    
    var loadError: Bool = false
    
    func load(completion: (Result<[Item], Error>) -> Void) {
        if !loadError {
            completion(.success([Item.preview]))
        } else {
            completion(.failure(JSONRepositoryError.cantFindJSON))
        }
    }
    
    var filePath: String = "empty"
    
    
}
