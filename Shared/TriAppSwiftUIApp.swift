//
//  TriApp.swift
//  Shared
//
//  Created by Steven Hendrie on 13/02/2021.
//

import SwiftUI

@main
struct TriApp: App {
    
    let environment = AppEnvironment.setup()
    
    init() {}
    
    var body: some Scene {
        WindowGroup {
            RootView()
                .inject(environment.dependencies)
                .buttonStyle(OutlineButton())
        }
    }
}

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
