//
//  AppState.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import SwiftUI

class AppState: ObservableObject {
    @Published var game = GameData()
    @Published var routing = ViewRouting()
    
    var staticData = StaticData()
    
}

extension AppState {
    
    struct GameData {
        var user: User
        var currentDay: Day
        var allEvents: [Event]
        var currentEvent: Event
        
        init() {
            self.user = User.empty
            self.currentDay = Day()
            self.allEvents = []
            self.currentEvent = Event.preview
        }
        
        struct Day {
            var date: Date = Date()
            var events: [Event] = []
        }
    }
}

extension AppState {
    struct ViewRouting {
        var app = RootView.Routing()
        var athleteCreation = AthleteCreationRootView.Routing()
        var event = EventRootView.Routing()
    }
}

extension AppState {
    class StaticData {
        var items: [Item] = []
        var athletes: [Athlete] = []
        var nationalities: [Nationality] = []
        var names: Names = Names()
    }
}


#if DEBUG
extension AppState {
    static var preview: AppState {
        let state = AppState()
        return state
    }
}
#endif
