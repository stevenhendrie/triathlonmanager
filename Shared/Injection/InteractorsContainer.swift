//
//  InteractorsContainer.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import Foundation

extension DependencyInjection {
    struct Interactors {
        
        let itemInteractor: ItemInteracting
        let nationalityInteractor: NationalityInteracting
        let athleteInteractor: AthleteInteracting
        let eventsInteractor: EventsInteracting
        let timelineInteractor: TimelineInteracting
        let raceInteractor: RaceInteracting
        let staticDataInteractor: StaticDataInteracting
        
        static var stub: Self {
            .init(itemInteractor: StubItemInteractor(),
                  nationalityInteractor: StubNationalityInteractor(),
                  athleteInteractor: StubAthleteInteractor(),
                  eventsInteractor: StubEventsInteractor(),
                  timelineInteractor: StubTimelineInteractor(),
                  raceInteractor: StubRaceInterator(),
                  staticDataInteractor: StubStaticDataInteractor())
        }
    }
}
