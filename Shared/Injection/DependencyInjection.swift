//
//  DependencyInjection.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import SwiftUI

struct DependencyInjection: EnvironmentKey {
    
    let appState: AppState
    let interactors: Interactors
        
    init(appState: AppState, interactors: Interactors) {
        self.appState = appState
        self.interactors = interactors
    }
    
    static var defaultValue: Self { Self.default }
    
    private static let `default` = Self(appState: AppState(), interactors: .stub)
}

extension EnvironmentValues {
    var injected: DependencyInjection {
        get { self[DependencyInjection.self] }
        set { self[DependencyInjection.self] = newValue }
    }
}

extension View {
    
    func inject(_ dependencies: DependencyInjection) -> some View {
        return self.environment(\.injected, dependencies).environmentObject(dependencies.appState)
    }
}

#if DEBUG
extension DependencyInjection {
    static var preview: Self {
        .init(appState: .preview, interactors: .stub)
    }
}
#endif
