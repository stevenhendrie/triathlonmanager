//
//  AthleteInteractor.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import Combine
import SwiftUI

protocol AthleteInteracting {
    func load(athletes: Binding<[Athlete]>)
}

struct AthleteInteractor: AthleteInteracting {
    
    let athleteRepository: AthleteRepositorying
    let nameRepository: NameRepositorying
    var appState: AppState
    
    private func loadNames() {
        
        let _ = nameRepository.load()
            .sink { (completion) in
                print(completion)
            } receiveValue: { names in
                appState.staticData.names = names
            }
    }
    
    func load(athletes: Binding<[Athlete]>) {
        
        var subscriptions = Set<AnyCancellable>()
        
        Just(())
            .setFailureType(to: Error.self)
            .flatMap { [athleteRepository] _ -> AnyPublisher<Bool, Error> in
                return athleteRepository.hasLoadedAthletes()
            }
            .flatMap { hasLoaded -> AnyPublisher<Void, Error> in
                if hasLoaded {
                    return Just(()).setFailureType(to: Error.self).eraseToAnyPublisher()
                } else {
                    return self.refreshAthletes()
                }
            }
            .flatMap { [athleteRepository] _ -> AnyPublisher<Int, Error> in
                return athleteRepository.count()
            }
            .flatMap { count -> AnyPublisher<Void, Error> in
                if count < 300 {
                    return self.createAthletes(count: 300 - count)
                } else {
                    return Just(()).setFailureType(to: Error.self).eraseToAnyPublisher()
                }
            }
            .flatMap { [athleteRepository] in
                athleteRepository.athletes()
            }
            .sink(receiveCompletion: { _ in }, receiveValue: {
                athletes.wrappedValue = $0
            })
            .store(in: &subscriptions)
    }
    
    private func refreshAthletes() -> AnyPublisher<Void, Error> {
        return athleteRepository
            .load()
            .flatMap { [athleteRepository] in
                athleteRepository.store(athletes: $0)
            }
            .eraseToAnyPublisher()
    }
    
    private func createAthletes(count: Int) -> AnyPublisher<Void, Error> {
        
        var athletes = [Athlete]()
        
        for _ in 0..<count {
            let gender = Athlete.Gender.allCases.randomElement()
            
            let firstName = gender == .male ? appState.staticData.names.boys.randomElement() : appState.staticData.names.girls.randomElement()
            let secondName = appState.staticData.names.surnames.randomElement()!
            let age = Double.random(in: 16...75)
            let time = age * 31556952
            let date = Date(timeIntervalSinceNow: -time)
            let professional = Int.random(in: 0...100) == 100
            let nationality = appState.staticData.nationalities.randomElement()
            
            let swim = Double.random(in: 200..<255)
            let bike = Double.random(in: 200..<255)
            let run = Double.random(in: 200..<255)
            
            athletes.append(Athlete(id: appState.staticData.athletes.count,
                           firstName: firstName!,
                           secondName: secondName,
                           gender: gender!,
                           dateOfBirth: date,
                           nationality: nationality!.id,
                           swimLevel: swim,
                           bikeLevel: bike,
                           runLevel: run,
                           professional: professional))
        }
        
        return Just<[Athlete]>(athletes)
            .flatMap { [athleteRepository] in
                athleteRepository.store(athletes: $0)
            }
            .eraseToAnyPublisher()
    }
}

struct StubAthleteInteractor: AthleteInteracting {

    func load(athletes: Binding<[Athlete]>) {
        
    }
}
