//
//  EventInteractor.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 15/02/2021.
//

import SwiftUI
import Combine

protocol EventsInteracting {
    func createEvents(start: Date, end: Date)
    func setCurrentDayEvents()
    
    func startEvent(_ event: Event)
    func eventsOnDate(_ date: Date, events: Binding<[Event]>)
    
}

struct EventsInteractor: EventsInteracting {
    
    let nationalityInteractor: NationalityInteracting
    
    let eventsRepository: EventsRepositorying
    let appState: AppState
    
    func createEvents(start: Date, end: Date) {
            
        var date = start
        while date <= end {
            
            appState.game.allEvents.append(createRandomEvent(date: date))
            
            date = Calendar.current.date(byAdding: .day, value: 1, to: date)!
        }
    }
    
    
    func startEvent(_ event: Event) {
        appState.game.currentEvent = event
        appState.game.currentEvent.state = .raceDay
        appState.routing.app.state = .event
    }
    
    func setCurrentDayEvents() {
        let events = retrieveEventsOnDate(appState.game.currentDay.date)
        appState.game.currentDay.events = events
    }
    
    func eventsOnDate(_ date: Date, events: Binding<[Event]>) {
        events.wrappedValue = retrieveEventsOnDate(date)
    }
    
    
   
}

extension EventsInteractor {
    
    private func retrieveEventsOnDate(_ date: Date) -> [Event] {
        return appState.game.allEvents.filter() { Calendar.current.isDate(date, inSameDayAs: $0.date) }
    }
    
    
    private func createRandomEvent(date: Date) -> Event {
        let id = appState.game.allEvents.count
        let name = "Event \(id)"
        
        let capacity = Int.random(in: 40..<100)
        let entrants = Int.random(in: Int(Double(capacity) * 0.5)..<capacity)
        
        var athletes: [Athlete] = []
        for _ in 0..<entrants {
            if let athlete = appState.staticData.athletes.randomElement() {
                if !athletes.contains(athlete) {
                    athletes.append(athlete)
                }
            }
        }
        
        let prizeMoney = Double.random(in: 10...1000)
        
        let nationality = appState.staticData.nationalities.randomElement()!
    
        let event = Event(id: id,
                          name: name,
                          date: date,
                          prizeMoney: prizeMoney,
                          capacity: capacity,
                          location: nationality,
                          type: .triathlon,
                          category: .sprint,
                          format: [ EventSection(discipline: .swim, distance: 0.75),
                                    EventSection(discipline: .bike, distance: 20),
                                    EventSection(discipline: .run, distance: 5)],
                          cutoff:  45 * 60)
        
        event.athletes = athletes
        return event
    }
}

struct StubEventsInteractor: EventsInteracting {
    
    func createEvents(start: Date, end: Date) {
        
    }
    
    func setCurrentDayEvents() {
        
    }
    
    func eventsOnDate(_ date: Date, events: Binding<[Event]>) {
        events.wrappedValue = [Event.preview, Event.preview]
    }
    
    func startEvent(_ event: Event) {
        
    }
}
