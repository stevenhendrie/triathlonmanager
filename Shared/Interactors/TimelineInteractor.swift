//
//  TimelineInteractor.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 18/02/2021.
//

import SwiftUI

protocol TimelineInteracting {
    func moveToNextDay()
    
    func isToday(date: Date, isToday: Binding<Bool>)
}

struct TimelineInteractor: TimelineInteracting {
    
    let appState: AppState
    let eventInteractor: EventsInteracting
    let raceInteractor: RaceInteracting
    
    func moveToNextDay() {
        
        for event in appState.game.currentDay.events {
            if event.state != .complete {
                eventInteractor.startEvent(event)
                raceInteractor.simRace(event: event)
                event.state = .complete
            }
        }
        
        appState.game.currentDay.date = Calendar.current.date(byAdding: .day, value: 1, to: appState.game.currentDay.date)!
        
        eventInteractor.setCurrentDayEvents()
        
        appState.routing.app.state = .game
    }
    
    func isToday(date: Date, isToday: Binding<Bool>) {
        isToday.wrappedValue = Calendar.current.isDate(date, inSameDayAs: appState.game.currentDay.date)
    }
}

struct StubTimelineInteractor: TimelineInteracting {
    
    func moveToNextDay() {
        
    }
    
    func isToday(date: Date, isToday: Binding<Bool>) {
        isToday.wrappedValue = Calendar.current.isDate(date, inSameDayAs: Date())
    }
}
