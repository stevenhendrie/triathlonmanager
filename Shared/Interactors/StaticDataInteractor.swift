//
//  StaticDataInteractor.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 28/02/2021.
//

import Combine

protocol StaticDataInteracting {
    func loadAllData() -> AnyPublisher<Bool,Error>
}

struct StaticDataInteractor: StaticDataInteracting {
    
    let athleteRepository: AthleteRepositorying
    let itemRepository: ItemRepositorying
    let nationalityRepository: NationalityRepositorying
    
    let appState: AppState
    
    func loadAllData() -> AnyPublisher<Bool,Error> {
    
        return Publishers.CombineLatest3(nationalityLoadedPublisher, athletesLoadedPublisher, itemLoadedPublisher)
            .map { (nationalityLoaded, athletesLoaded, itemLoaded) -> Bool in
                return nationalityLoaded && athletesLoaded && itemLoaded
            }
            .eraseToAnyPublisher()
    }
    
    private var itemLoadingPublisher: AnyPublisher<[Item], Error> {
        return nationalityRepository
            .hasLoaded()
            .flatMap { [itemRepository] hasLoaded -> AnyPublisher<[Item],Error> in
                if hasLoaded {
                    return itemRepository.items(category: nil)
                } else {
                    return itemRepository.load()
                }
            }
            .eraseToAnyPublisher()
    }
    
    private var itemLoadedPublisher: AnyPublisher<Bool, Error> {
        return itemLoadingPublisher
            .map { $0.count > 0 }
            .eraseToAnyPublisher()
    }
  
    
    private var nationalityLoadingPublisher: AnyPublisher<[Nationality], Error> {
        return nationalityRepository
            .hasLoaded()
            .flatMap { [nationalityRepository] hasLoaded -> AnyPublisher<[Nationality],Error> in
                if hasLoaded {
                    return nationalityRepository.nationalities()
                } else {
                    return nationalityRepository.load()
                }
            }
            .eraseToAnyPublisher()
    }
    
    private var nationalityLoadedPublisher: AnyPublisher<Bool, Error> {
        return nationalityLoadingPublisher
            .map { $0.count > 0 }
            .eraseToAnyPublisher()
    }
    
    
    private var athletesLoadingPublisher: AnyPublisher<[Athlete], Error> {
        return athleteRepository
            .hasLoadedAthletes()
            .flatMap { [athleteRepository] hasLoaded -> AnyPublisher<[Athlete],Error> in
                if hasLoaded {
                    return athleteRepository.athletes()
                } else {
                    return athleteRepository.load()
                }
            }
            .eraseToAnyPublisher()
    }
    
    private var athletesLoadedPublisher: AnyPublisher<Bool, Error> {
        return athletesLoadingPublisher
            .map { $0.count > 0 }
            .eraseToAnyPublisher()
    }
   
}

struct StubStaticDataInteractor: StaticDataInteracting {
    func loadAllData() -> AnyPublisher<Bool, Error> {
        return Just(true)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
    
    
}
