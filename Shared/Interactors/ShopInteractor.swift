//
//  ShopInteractor.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import Foundation

protocol ItemInteractor {
    func loadShopItems()
}

struct ShopItemsInteractor: ItemInteractor {
    
    let appState: AppState
    
    func loadShopItems() {
        
        
    }
}
