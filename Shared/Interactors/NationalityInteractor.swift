//
//  NationalityInteractor.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 14/02/2021.
//

import Combine
import SwiftUI

protocol NationalityInteracting {
    func nationalities(_ nationalities: Binding<[Nationality]>)
}

struct NationalityInteractor: NationalityInteracting {
    
    let nationalityRepository: NationalityRepositorying
    let appState: AppState
    
    func nationalities(_ nationalities: Binding<[Nationality]>) {
        
        let _ = nationalityRepository.nationalities()
            .sink(receiveCompletion: { _ in }, receiveValue: {
                nationalities.wrappedValue = $0
            })
    }
    
}

struct StubNationalityInteractor: NationalityInteracting {
    
    func nationalities(_ nationalities: Binding<[Nationality]>) {
        
    }
}
