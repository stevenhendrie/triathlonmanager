//
//  StaticDataInteractor.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import Combine
import SwiftUI

protocol ItemInteracting {
    func items(_ items: Binding<[Item]>, category: Item.Category)
    func canAffordItem(_ item: Item, bind: Binding<Bool>)
    func buyItem(_ item: Item)
}

struct ShopItemInteractor: ItemInteracting {
    
    let itemRepository: ItemRepository
    
    let appState: AppState
    
    
    func items(_ items: Binding<[Item]>, category: Item.Category) {
        
        let _ = itemRepository.items(category: category)
            .sink(receiveCompletion: { _ in }, receiveValue: {
                items.wrappedValue = $0
            })
    }
    
    func canAffordItem(_ item: Item, bind: Binding<Bool>) {
        bind.wrappedValue = canAffordItem(item)
    }
    
    private func canAffordItem(_ item: Item) -> Bool {
        return appState.game.user.money >= item.price
    }
    
    func buyItem(_ item: Item) {
        if canAffordItem(item) {
            appState.game.user.inventory.items.append(item)
            appState.game.user.money -= item.price
        }
    }
}

struct StubItemInteractor: ItemInteracting {
    
    func items(_ items: Binding<[Item]>, category: Item.Category) {
        
    }
    
    func canAffordItem(_ item: Item, bind: Binding<Bool>) {
        
    }
    
    func buyItem(_ item: Item) {
        
    }
}
