//
//  RaceInteractor.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 18/02/2021.
//

import SwiftUI

protocol RaceInteracting {
    func startRace(event: Event)
    func simRace(event: Event)
}

struct RaceInteractor: RaceInteracting {
    
    var appState: AppState
    
    func startRace(event: Event) {
        
        setupRace(event: event)
        
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { timer in
            
            updateRaceState(event: event, timeIncrease: 50)
            
            if isRaceComplete(event: event) {
                timer.invalidate()
                event.state = .complete
            }
            
        }
    }
    
    func simRace(event: Event) {
        setupRace(event: event)
        
        updateRaceState(event: event, timeIncrease: event.cutoff)
        event.state = .complete
    }
}

extension RaceInteractor {
    
    private func setupRace(event: Event) {
        event.state = .inProgress
        
        for athlete in event.athletes {
            event.results[athlete.id] = Event.Progress()
            event.results[athlete.id]?.state = .started
        }
    }
    
    private func updateRaceState(event: Event, timeIncrease: Double) {
        
        for athlete in event.athletes {
            updateAthlete(athlete, event: event, timeIncrease: timeIncrease)
        }
        
        if isRaceComplete(event: event) {
            if let time = event.results.map({ $0.value.time }).max() {
                event.elapsedTime = time
            }
        } else {
            event.elapsedTime += timeIncrease
        }
    }
    
    private func updateAthlete(_ athlete: Athlete, event: Event, timeIncrease: Double) {
        
        let currentProgress = event.results[athlete.id] ?? Event.Progress()
        
        if currentProgress.state == .completed {
            return
        }
    
        var updatedProgress = currentProgress
        
        var timeLeft = timeIncrease
        
        repeat {
            
            if updatedProgress.distanceComplete >= event.totalDistance {
                updatedProgress.state = .completed
                break
            }
            
            let distanceLeftInSection = event.distanceLeftInCurrentSection(updatedProgress.distanceComplete)
            
            let currentSection = event.sectionAtDistance(updatedProgress.distanceComplete)
            let travelSpeed = currentTravelSpeed(athlete: athlete, section: currentSection)
            let maxDistanceCanTravelInSection = travelSpeed * timeLeft
            
            if maxDistanceCanTravelInSection > distanceLeftInSection {
                
                let timeTaken = distanceLeftInSection / travelSpeed
                timeLeft -= timeTaken
                
                updatedProgress.time += timeTaken
                updatedProgress.distanceComplete += distanceLeftInSection
            } else {
                updatedProgress.distanceComplete += maxDistanceCanTravelInSection
                updatedProgress.time += timeLeft
                timeLeft = 0
            }
            
            if updatedProgress.time >= event.cutoff && updatedProgress.distanceComplete < event.totalDistance{
                let timeDiff = updatedProgress.time - event.cutoff
                
                if timeDiff > 0 {
                    updatedProgress.distanceComplete -= travelSpeed * timeDiff
                }
                
                updatedProgress.time -= timeDiff
                updatedProgress.state = .didNotFinish
                timeLeft = 0
            }
            
        } while (timeLeft > 0)
        
        event.results[athlete.id] = updatedProgress
    }
    

    private func currentTravelSpeed(athlete: Athlete, section: EventSection) -> Double {
        var travelSpeed = 0.0 //M/S
        switch(section.discipline) {
            case .swim:
                travelSpeed = (athlete.swimLevel / 255.0) * 2 //MAX Swim Speed = 2m/s
            case .bike:
                travelSpeed = (athlete.bikeLevel / 255.0) * 16.6667 //Max Bike Speed = 16m/s = 60km/h
            case .run:
                travelSpeed = (athlete.runLevel / 255.0) * 5.556 //Max Bike Speed = 5.556m/s = 20km/h
        }
        
        return travelSpeed / 1000.0
    }
    
    
    private func isRaceComplete(event: Event) -> Bool {
        
        for athlete in event.athletes {
            if let progress = event.results[athlete.id],
               (progress.state == .preRace || progress.state == .started) {
                return false
            }
        }
        return true
    }
}


struct StubRaceInterator: RaceInteracting {
    
    func startRace(event: Event) {
        
    }
    
    func simRace(event: Event) {
        
    }
}
