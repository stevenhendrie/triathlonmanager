import csv
import json
import uuid
 

def numerify(row):
    for k, v in list(row.items()):
        try:
            row[k] = float(v)
            row[k] = int(v)
        except ValueError:
            pass
    
def str2True(v):
   return str(v).lower() in ("yes", "true")
    
def str2False(v):
    return str(v).lower() in ("no", "false")

def boolify(row):
    for k, v in list(row.items()):
        try:
            if str2True(v):
                row[k] = True
            elif str2False(v):
                row[k] = False
        except ValueError:
            pass
        
def make_json(csvFilePath, jsonFilePath, ignored = []):
     
    # create a dictionary
    data = []
     
    # Open a csv reader called DictReader
    with open(csvFilePath, encoding='utf-8') as csvf:
        csvReader = csv.DictReader(csvf)
         
        # Convert each row into a dictionary 
        # and add it to data
        for rows in csvReader:
             
            # Assuming a column named 'No' to
            # be the primary key
            # rows['ID'] = str(uuid.uuid4())
            for ignore in ignored:
                rows.pop(ignore)
            
            numerify(rows)
            boolify(rows)
            data.append(rows)
 
    # Open a json writer, and use the json.dumps() 
    # function to dump data
    
    with open(jsonFilePath, 'w', encoding='utf-8') as jsonf:
        jsonf.write(json.dumps(data, indent=4))
         
# Driver Code
 
# Call the make_json function
make_json(r'../Data/Data-Athletes.csv', r'../athletes.json' , ["nationalityName"])
make_json(r'../Data/Data-Nationalities.csv', r'../nationalities.json')
make_json(r'../Data/Data-Items.csv', r'../items.json')
