//
//  AppEnvironment.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import Foundation

struct AppEnvironment {
    let dependencies: DependencyInjection
}

extension AppEnvironment {
    static func setup() -> AppEnvironment {
        let appState = AppState()
        
        let persistentStore = CoreDataStack(version: CoreDataStack.Version.actual)

        let itemRepository = ItemRepository(persistentStore: persistentStore)
        
        let athleteRepository = AthleteRepository(persistentStore: persistentStore)
        
        let nationalityRepository = NationalityRepository(persistentStore: persistentStore)
        
        let eventRepositry = EventsRepository()
        let nameRepository = NameRepository()
        
        
        let shopItemInteractor = ShopItemInteractor(itemRepository: itemRepository, appState: appState)
        let nationalityInteractor = NationalityInteractor(nationalityRepository: nationalityRepository, appState: appState)
        let athleteInteractor = AthleteInteractor(athleteRepository: athleteRepository, nameRepository: nameRepository, appState: appState)

        let eventsInteractor = EventsInteractor(nationalityInteractor: nationalityInteractor, eventsRepository: eventRepositry, appState: appState)
    
        let raceInteractor = RaceInteractor(appState: appState)
        let timelineInteractor = TimelineInteractor(appState: appState, eventInteractor: eventsInteractor, raceInteractor: raceInteractor)
        
        let staticDataInteractor = StaticDataInteractor(athleteRepository: athleteRepository, itemRepository: itemRepository, nationalityRepository: nationalityRepository, appState: appState)
        
        let interactors = DependencyInjection.Interactors(itemInteractor: shopItemInteractor,
                                                          nationalityInteractor: nationalityInteractor,
                                                          athleteInteractor: athleteInteractor,
                                                          eventsInteractor: eventsInteractor,
                                                          timelineInteractor: timelineInteractor,
                                                          raceInteractor: raceInteractor,
                                                          staticDataInteractor: staticDataInteractor)
        
        return AppEnvironment(dependencies: DependencyInjection(appState: appState, interactors: interactors))
    }
    
    func loadSavedData() {
//        dependencies.interactors.itemInteractor.loadItems()
//        dependencies.interactors.nationalityInteractor.loadNationalities()
//        dependencies.interactors.athleteInteractor.loadAthletes()
        dependencies.interactors.eventsInteractor.createEvents(start: Date(), end: Date(timeIntervalSinceNow: 10 * 24 * 60 * 60));
        
        
    }
}
