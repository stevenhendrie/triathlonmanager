//
//  UserDefaultsRepository.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import Foundation
import Combine
enum UserDefaultsRepositoryError: Error {
    case cantDecodeData
    case cantEncodeObject
    case cantFindData
}

protocol UserDefaultsRepository {
    var userDefaultsKey: String { get }
}

extension UserDefaultsRepository {
    
    func load<Value>(completion: ((Result<Value, Error>) -> ())) where Value: Codable {
        
        guard let objectData = UserDefaults.standard.object(forKey: userDefaultsKey) as? Data else  { completion(.failure(UserDefaultsRepositoryError.cantFindData)); return }
        guard let loadedObject = try? JSONDecoder().decode(Value.self, from: objectData) else { completion(.failure(UserDefaultsRepositoryError.cantDecodeData)); return }
        
        completion(.success(loadedObject))
    }
    
    func set<Value>(value: Value) where Value: Codable {
        
        guard let encoded = try? JSONEncoder().encode(value) else { return }
        UserDefaults.standard.set(encoded, forKey: userDefaultsKey)
    }
    
    func remove() {
        UserDefaults.standard.removeObject(forKey: userDefaultsKey)
    }
}
