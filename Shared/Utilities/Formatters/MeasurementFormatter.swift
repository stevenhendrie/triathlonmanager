//
//  MeasurementFormatter.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 16/02/2021.
//

import Foundation

extension MeasurementFormatter {
    
    static let distance: MeasurementFormatter = {
        var measurementFormatter = MeasurementFormatter()
        measurementFormatter.unitOptions = .providedUnit
        measurementFormatter.locale = Locale(identifier: "fr_FR")
        measurementFormatter.unitStyle = .short
        measurementFormatter.numberFormatter = NumberFormatter.distance
        
        return measurementFormatter
    }()
}
