//
//  Number.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 16/02/2021.
//

import Foundation

extension FloatingPoint {
    var whole: Self { modf(self).0 }
    var fraction: Self { modf(self).1 }
}

extension Double {
    
    var distanceString: String {
        return MeasurementFormatter.distance.string(from: Measurement(value: self, unit: UnitLength.kilometers))
    }
    
    var timeString: String {
        let hours = self / 3600
        let minutes = (self / 60).truncatingRemainder(dividingBy: 60)
        let seconds = self.truncatingRemainder(dividingBy: 60)
        let milliseconds = self.fraction * 1000

        
        var stringTime = ""
        if let hoursTime = NumberFormatter.timeComponent.string(for: hours.whole), hours > 1 {
            stringTime += hoursTime
            stringTime += ":"
        }
        if let minuteTime = NumberFormatter.timeComponent.string(for: minutes.whole), minutes > 1 {
            stringTime += minuteTime
            stringTime += ":"
        }
        if let secondsTime = NumberFormatter.timeComponent.string(for: seconds) {
                stringTime += secondsTime
                stringTime += ":"
        }
        
        if let millisecondTime = NumberFormatter.timeComponent.string(for: milliseconds) {
                stringTime += millisecondTime
        }
        
        return stringTime
    }
}
