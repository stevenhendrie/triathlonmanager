//
//  NumberFormatters.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 15/02/2021.
//

import Foundation

extension NumberFormatter {
    
    static let currency: NumberFormatter = {
        var numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale.current
        numberFormatter.numberStyle = .currency
        numberFormatter.maximumFractionDigits = 0
        return numberFormatter
    }()
    
    
    static let distance: NumberFormatter = {
        var numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale.current
        numberFormatter.maximumFractionDigits = 2
//        numberFormatter.maximumSignificantDigits = 5
        return numberFormatter
    }()
    
    static let timeComponent: NumberFormatter = {
        var numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale.current
        numberFormatter.maximumSignificantDigits = 2
        numberFormatter.maximumFractionDigits = 0
        return numberFormatter
    }()
}
