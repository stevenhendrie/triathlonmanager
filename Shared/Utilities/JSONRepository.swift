//
//  JSONRepository.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import Foundation
import Combine

enum JSONRepositoryError: Error {
    case cantFindJSON
}

protocol JSONRepository {
    var filePath: String { get }
}

extension JSONRepository {
        
    func loadFromFile<Value: Decodable>() -> AnyPublisher<Value, Error> {
        
        do {
            if let bundlePath = Bundle.main.path(forResource: filePath, ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                
                let decoder = JSONDecoder()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                decoder.dateDecodingStrategy = .formatted(dateFormatter)
                
                
                return Just(jsonData)
                    .decode(type: Value.self, decoder: decoder)
                    .eraseToAnyPublisher()
            }
        } catch {
            return Fail(error: JSONRepositoryError.cantFindJSON)
                .eraseToAnyPublisher()
        }
            
            return Fail(error: JSONRepositoryError.cantFindJSON)
                .eraseToAnyPublisher()
    }
}
