//
//  Date+Extensions.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 18/02/2021.
//

import Foundation

extension Date {
    
    var weeksOfMonth: [Date] {
        
        let calendar = Calendar.current
        guard let monthInterval = calendar.dateInterval(of: .month, for: self) else { return [] }
        var dates: [Date] = []
        dates.append(monthInterval.start)

        calendar.enumerateDates(
            startingAfter: monthInterval.start,
            matching: DateComponents(hour: 0, minute: 0, second: 0, weekday: calendar.firstWeekday),
            matchingPolicy: .nextTime
        ) { date, _, stop in
            if let date = date {
                if date < monthInterval.end {
                    dates.append(date)
                } else {
                    stop = true
                }
            }
        }

        return dates
    }
    
    var daysOfWeek: [Date] {
        let calendar = Calendar.current
        
        guard let weekInterval = calendar.dateInterval(of: .weekOfYear, for: self) else { return [] }
            
        var dates: [Date] = []
        dates.append(weekInterval.start)

        calendar.enumerateDates(
            startingAfter: weekInterval.start,
            matching: DateComponents(hour: 0, minute: 0, second: 0),
            matchingPolicy: .nextTime
        ) { date, _, stop in
            if let date = date {
                if date < weekInterval.end {
                    dates.append(date)
                } else {
                    stop = true
                }
            }
        }
        return dates
    }
    
}
