//
//  AthleteRepository.swift
//  TriAppSwiftUI (iOS)
//
//  Created by Steven Hendrie on 24/02/2021.
//

import CoreData
import Combine

protocol AthleteRepositorying {
    func load() -> AnyPublisher<[Athlete], Error>
    
    func hasLoadedAthletes() -> AnyPublisher<Bool, Error>
    func store(athletes: [Athlete]) -> AnyPublisher<Void, Error>
    func athletes() -> AnyPublisher<[Athlete], Error>
    func count() -> AnyPublisher<Int, Error>
}


struct AthleteRepository: AthleteRepositorying, JSONRepository {
    
    let persistentStore: PersistentStore
    
    var filePath: String = "athletes"
    
    func load() -> AnyPublisher<[Athlete], Error> {
        return loadFromFile()
            .eraseToAnyPublisher()
    }
    
    func hasLoadedAthletes() -> AnyPublisher<Bool, Error> {
        let fetchRequest = AthleteMO.justOneAthlete()
        return persistentStore
            .count(fetchRequest)
            .map { $0 > 0 }
            .eraseToAnyPublisher()
    }
    
    func store(athletes: [Athlete]) -> AnyPublisher<Void, Error> {
        return persistentStore
            .update { context in
                athletes.forEach {
                    $0.store(in: context)
                }
            }
    }
    
    func athletes() -> AnyPublisher<[Athlete], Error> {
        let fetchRequest = AthleteMO.athletes()
        return persistentStore
            .fetch(fetchRequest) {
                Athlete(managedObject: $0)
            }
            .eraseToAnyPublisher()
    }
    
    func count() -> AnyPublisher<Int, Error> {
        return athletes()
            .count()
            .eraseToAnyPublisher()
    }
}

extension AthleteMO {
    
    static func justOneAthlete() -> NSFetchRequest<AthleteMO> {
        let request = newFetchRequest()
        request.predicate = NSPredicate(format: "id == %@", "1")
        request.fetchLimit = 1
        return request
    }
    
    static func athletes() -> NSFetchRequest<AthleteMO> {
        let request = newFetchRequest()
        request.predicate = NSPredicate(value: true)
        request.sortDescriptors = [NSSortDescriptor(key: "price", ascending: true)]
        request.fetchBatchSize = 10
        return request
    }
}
