//
//  NameRepository.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 16/02/2021.
//

import Combine

protocol NameRepositorying: JSONRepository {
    func load() -> AnyPublisher<Names, Error>
}

struct NameRepository: NameRepositorying {
    
    var filePath: String = "names"

    func load() -> AnyPublisher<Names, Error> {
        return loadFromFile()
            .eraseToAnyPublisher()
    }
}
