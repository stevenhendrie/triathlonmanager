//
//  EventsRepository.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 15/02/2021.
//

import Foundation

protocol EventsRepositorying {
    
}

struct EventsRepository: EventsRepositorying {
    
}

struct StubEventsRepositry: EventsRepositorying {
    
}
