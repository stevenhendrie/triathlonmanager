//
//  NationalityRepository.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 24/02/2021.
//

import CoreData
import Combine

protocol NationalityRepositorying {
    func load() -> AnyPublisher<[Nationality], Error>
    
    func hasLoaded() -> AnyPublisher<Bool, Error>
    func store(nationalities: [Nationality]) -> AnyPublisher<Void, Error>
    func nationalities() -> AnyPublisher<[Nationality], Error>
}

struct NationalityRepository: NationalityRepositorying, JSONRepository {
    
    let persistentStore: PersistentStore
    
    var filePath: String = "nationalities"
    
    func load() -> AnyPublisher<[Nationality], Error> {
        return loadFromFile()
            .eraseToAnyPublisher()
    }
    
    func hasLoaded() -> AnyPublisher<Bool, Error> {
        let fetchRequest = NationalityMO.justOneItem()
        return persistentStore
            .count(fetchRequest)
            .map { $0 > 0 }
            .eraseToAnyPublisher()
    }
    
    func store(nationalities: [Nationality]) -> AnyPublisher<Void, Error> {
        return persistentStore
            .update { context in
                nationalities.forEach {
                    $0.store(in: context)
                }
            }
    }
    
    func nationalities() -> AnyPublisher<[Nationality], Error> {
        let fetchRequest = NationalityMO.nationalities()
        return persistentStore
            .fetch(fetchRequest) {
                Nationality(managedObject: $0)
            }
            .eraseToAnyPublisher()
    }
    
}

extension NationalityMO {
    
    static func justOneItem() -> NSFetchRequest<NationalityMO> {
        let request = newFetchRequest()
        request.predicate = NSPredicate(format: "id == %@", "1")
        request.fetchLimit = 1
        return request
    }
    
    static func nationalities() -> NSFetchRequest<NationalityMO> {
        let request = newFetchRequest()
        request.predicate = NSPredicate(value: true)
        request.sortDescriptors = [NSSortDescriptor(key: "price", ascending: true)]
        request.fetchBatchSize = 10
        return request
    }
}
