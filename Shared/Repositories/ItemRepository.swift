//
//  ItemDBRepository.swift
//  TriAppSwiftUI (iOS)
//
//  Created by Steven Hendrie on 24/02/2021.
//

import CoreData
import Combine

protocol ItemRepositorying {
    func load() -> AnyPublisher<[Item], Error>
    
    func hasLoadedItems() -> AnyPublisher<Bool, Error>
    func store(items: [Item]) -> AnyPublisher<Void, Error>
    func items(category: Item.Category?) -> AnyPublisher<[Item], Error>
}

struct ItemRepository: ItemRepositorying, JSONRepository {
    
    let persistentStore: PersistentStore
    
    var filePath: String = "items"
    
    func load() -> AnyPublisher<[Item], Error> {
        return loadFromFile()
            .eraseToAnyPublisher()
    }
    
    func hasLoadedItems() -> AnyPublisher<Bool, Error> {
        
        let fetchRequest = ItemMO.justOneItem()
        return persistentStore
            .count(fetchRequest)
            .map { $0 > 0 }
            .eraseToAnyPublisher()
    }
    
    func store(items: [Item]) -> AnyPublisher<Void, Error> {
        return persistentStore
            .update { context in
                items.forEach {
                    $0.store(in: context)
                }
            }
    }
    
    func items(category: Item.Category? = nil) -> AnyPublisher<[Item], Error> {
        let fetchRequest = ItemMO.items(category: category)
        return persistentStore
            .fetch(fetchRequest) {
                Item(managedObject: $0)
            }
            .eraseToAnyPublisher()
    }
}


extension ItemMO {
    
    static func justOneItem() -> NSFetchRequest<ItemMO> {
        let request = newFetchRequest()
        request.predicate = NSPredicate(format: "id == %@", "1")
        request.fetchLimit = 1
        return request
    }
    
    static func items(category: Item.Category? = nil) -> NSFetchRequest<ItemMO> {
        let request = newFetchRequest()
        if let category = category {
            request.predicate = NSPredicate(format: "category CONTAINS[cd] %@", category.rawValue)
        } else {
            request.predicate = NSPredicate(value: true)
        }
        request.sortDescriptors = [NSSortDescriptor(key: "price", ascending: true)]
        request.fetchBatchSize = 10
        return request
    }
}
