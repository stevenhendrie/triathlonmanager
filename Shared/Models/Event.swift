//
//  Event.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 15/02/2021.
//

import Foundation

class Event: Identifiable, ObservableObject {
    
    let id: Int
    let name: String
    let date: Date
    let prizeMoney: Double
    let capacity: Int
    let location: Nationality
    let type: EventType
    let category: DistanceCategory
    let format: [EventSection]
    let cutoff: Double
    
    enum State {
        case scheduled
        case raceDay
        case inProgress
        case complete
    }
    
    @Published var state: State = .scheduled
    
    var athletes: [Athlete] = []
    
    
    @Published var elapsedTime: Double = 0.0
    
    struct Progress {
        var distanceComplete: Double = 0.0
        var time: Double = 0.0
        enum State {
            case preRace
            case didNotStart
            case started
            case completed
            case didNotFinish
        }
        var state: State = .preRace
    }
    
    typealias EventResult = [Int: Progress]
    @Published var results: EventResult = [:]
    
    
    init(id: Int, name: String, date: Date, prizeMoney: Double, capacity: Int, location: Nationality, type: EventType, category: DistanceCategory, format: [EventSection], cutoff: Double) {
        self.id = id
        self.name = name
        self.date = date
        self.prizeMoney = prizeMoney
        self.capacity = capacity
        self.location = location
        self.type = type
        self.category = category
        self.format = format
        self.cutoff = cutoff
    }
    
    
    var totalDistance: Double {
        return format.reduce(0) { (result, next) -> Double in
            return result + next.distance
        }
    }
    
    func sectionAtDistance(_ distance: Double) -> EventSection {
        var distanceTravelled = 0.0
        for section in format {
            if distance < (distanceTravelled + section.distance) {
                return section
            } else {
                distanceTravelled += section.distance
            }
        }
        
        return format.first!
    }
    
    private func distanceAtStartOfSection(_ id: Int) -> Double {
        var runningDistance = 0.0
        for section in 0...id {
            if section == id {
                return runningDistance
            } else {
                runningDistance += format[section].distance
            }
        }
        return 0.0
    }
    
    private func distanceAtEndOfSection(_ id: Int) -> Double {
        var runningDistance = 0.0
        for section in 0...id {
            if section == id {
                return runningDistance + format[section].distance
            } else {
                runningDistance += format[section].distance
            }
        }
        return 0.0
    }
    
    func distanceLeftInCurrentSection(_ distanceTravelled: Double ) -> Double {
        for sectionID in 0..<format.count {
            let distanceAtStart = distanceAtStartOfSection(sectionID)
            let distanceAtEnd = distanceAtEndOfSection(sectionID)
            if distanceAtStart..<distanceAtEnd ~= distanceTravelled {
                return distanceAtEnd - distanceTravelled
            }
        }
        
        return 0
    }
    
    func distanceLeftInRace(_ distanceTravelled: Double) -> Double {
        return totalDistance - distanceTravelled
    }
    
    #if DEBUG
    static let preview: Event = Event(id: 1,
                                      name: "Preview Event",
                                      date: Date(),
                                      prizeMoney: 500,
                                      capacity: 30,
                                      location: Nationality.preview,
                                      type: .triathlon,
                                      category: .sprint,
                                      format: [EventSection(discipline: .swim, distance: 0.75),
                                               EventSection(discipline: .bike, distance: 20),
                                               EventSection(discipline: .run, distance: 5)],
                                      cutoff: 3 * 60 * 60)
    #endif
}

enum DistanceCategory: String {
    case superSprint
    case sprint
    case olympic
    case half
    case full
}

enum EventType: String {
    case swim
    case run
    case ride
    case duathlon
    case swimRun
    case triathlon
}

struct EventSection: Hashable {
    let discipline: Discipline
    let distance: Double
}

