//
//  Inventory.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 14/02/2021.
//

import Foundation

struct Inventory: Codable {
    var items: [Item] = []
    
    static var empty = Inventory()
}
