//
//  Nationality.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 14/02/2021.
//

import Foundation

struct Nationality: Codable, Hashable, Identifiable {
    let id: Int
    let country: String
    let nationality: String
    
    #if DEBUG
    static let preview: Nationality = Nationality(id: 1, country: "Scotland", nationality: "Scottish")
    #endif
}
