//
//  Names.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 16/02/2021.
//

import Foundation

struct Names: Decodable {
    var girls: [String] = []
    var boys: [String] = []
    var surnames: [String] = []
}
