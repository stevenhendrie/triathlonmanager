//
//  Discipline.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 16/02/2021.
//

import Foundation

enum Discipline: String {
    case swim
    case bike
    case run
}
