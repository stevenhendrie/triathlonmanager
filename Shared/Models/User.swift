//
//  User.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 14/02/2021.
//

import Foundation

struct User: Codable {
    var athleteInfo : Athlete
    var inventory: Inventory
    var money: Double
    
    static var empty: User = {
        return User(athleteInfo: Athlete.preview, inventory: Inventory(), money: 1000)
    }()
}
