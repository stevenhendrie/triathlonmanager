//
//  Athlete.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import Foundation

struct Athlete: Codable, Identifiable, Equatable {
    
    let id: Int
    let firstName: String
    let secondName: String
    var fullName: String {
        return "\(firstName) \(secondName)"
    }
    
    let gender: Gender
    let dateOfBirth: Date
    let nationality: NationalityReference
    
    let swimLevel: Double
    let bikeLevel: Double
    let runLevel: Double
    
    let professional: Bool
    
    enum Gender: String, Codable, CaseIterable {
        case male
        case female
    }
    
    var age: Int {
        let years = Calendar.current.dateComponents([.year], from: dateOfBirth, to: Date()).year ?? 0
        return years //TODO: Change to be in-app date.
    }
    
    var ageGroup: AgeGroup {
        if professional { return .elite }
        return AgeGroup.from(age: self.age)
    }
    
    typealias NationalityReference = Int
    
    #if DEBUG
    static let preview = Athlete(id: 1, firstName: "Steven", secondName: "Hendrie", gender: .male, dateOfBirth: Date(), nationality: 1, swimLevel: 5, bikeLevel: 5, runLevel: 10, professional: false)
    #endif
    
    
}



