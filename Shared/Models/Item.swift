//
//  Item.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import Foundation

struct Item: Codable, Equatable, Identifiable {
    let id: Int
    let name: String
    let price: Double
    let category: Category
    
    #if DEBUG
    static let preview: Item = Item(id: 1, name: "Preview Item", price: 200, category: .frame)
    #endif
}


extension Item {
    enum Category: String, Codable, CaseIterable {
        case frame
        case wheel
    }
}
