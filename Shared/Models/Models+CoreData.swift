//
//  Models+CoreData.swift
//  TriAppSwiftUI (iOS)
//
//  Created by Steven Hendrie on 24/02/2021.
//

import Foundation
import CoreData

extension ItemMO: ManagedEntity {}
extension AthleteMO: ManagedEntity {}
extension NationalityMO: ManagedEntity {}


extension Item {
    
    @discardableResult
    func store(in context: NSManagedObjectContext) -> ItemMO? {
        guard let item = ItemMO.insertNew(in: context)
            else { return nil }
        
        item.name = name
        item.price = price
        item.category = category.rawValue
        item.id = Int32(id)
        
        return item
    }

    init?(managedObject: ItemMO) {
        
        guard let name = managedObject.name,
              let category = managedObject.category,
              let catEnum = Category(rawValue: category)
            else { return nil }
        
        self.init(id: Int(managedObject.id),
                  name: name,
                  price: managedObject.price,
                  category: catEnum)
    }
}

extension Athlete {
    
    @discardableResult
    func store(in context: NSManagedObjectContext) -> AthleteMO? {
        guard let athlete = AthleteMO.insertNew(in: context)
            else { return nil }
        
        athlete.firstName = firstName
        athlete.secondName = secondName
        athlete.gender = gender.rawValue
        athlete.dateOfBirth = dateOfBirth
        athlete.swimLevel = swimLevel
        athlete.bikeLevel = bikeLevel
        athlete.runLevel = runLevel
        athlete.professional = professional
        
        return athlete
    }

    init?(managedObject: AthleteMO) {
        
        guard let firstName = managedObject.firstName,
              let secondName = managedObject.secondName,
              let gender = managedObject.gender,
              let genderEnum = Gender(rawValue: gender)
            else { return nil }
        
        self.init(id: Int(managedObject.id),
                  firstName: firstName,
                  secondName: secondName,
                  gender: genderEnum,
                  dateOfBirth: Date(),
                  nationality: 1,
                  swimLevel: managedObject.swimLevel,
                  bikeLevel: managedObject.bikeLevel,
                  runLevel: managedObject.runLevel,
                  professional: managedObject.professional)
       
    }
}

extension Nationality {
    
    @discardableResult
    func store(in context: NSManagedObjectContext) -> NationalityMO? {
        guard let nationalityInfo = NationalityMO.insertNew(in: context)
            else { return nil }
        
        nationalityInfo.id = Int32(id)
        nationalityInfo.country = country
        nationalityInfo.nationality = nationality
        
        return nationalityInfo
    }

    init?(managedObject: NationalityMO) {
        
        guard let country = managedObject.country,
              let nationality = managedObject.nationality
            else { return nil }
        
        self.init(id: Int(managedObject.id),
                  country: country,
                  nationality: nationality)
       
    }
}
