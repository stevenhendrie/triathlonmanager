//
//  AgeGroup.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 16/02/2021.
//

import Foundation

extension Athlete {
    enum AgeGroup: String, CaseIterable {
        case all
        case elite
        case underNineteen
        case twenty
        case twentyfive
        case thirty
        case thirtyfive
        case fourty
        case fourtyfive
        case fifty
        case fiftyfive
        case sixty
        case sixtyfive
        case overSeventy
        
        static func from(age: Int) -> AgeGroup {
            switch (age) {
            case 0...19:
                return .underNineteen
            case 20...24:
                return .twenty
            case 25...29:
                return .twentyfive
            case 30...34:
                return .thirty
            case 35...39:
                return .thirtyfive
            case 40...44:
                return .fourty
            case 45...49:
                return .fourtyfive
            case 50...54:
                return .fifty
            case 55...59:
                return .fiftyfive
            case 60...64:
                return .sixty
            case 65...69:
                return .sixtyfive
            default:
                return .overSeventy
            }
        }
    }
}
