//
//  View+Extensions.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 14/02/2021.
//

import SwiftUI

extension View {
    @ViewBuilder func isHidden(_ hidden: Bool, remove: Bool = false) -> some View {
        if hidden {
            if !remove {
                self.hidden()
            }
        } else {
            self
        }
    }
}

extension View {
    func Print(_ vars: Any...) -> some View {
        for v in vars { print(v) }
        return EmptyView()
    }
}


extension View {
    
    func uiPreview(width: Double, height: Double) -> some View {
        
//        let dynamicTypeSizes: [ContentSizeCategory] = [.extraSmall, .large, .extraExtraExtraLarge]

            /// Filter out "base" to prevent a duplicate preview.
//        let localizations = Bundle.main.localizations.map(Locale.init).filter { $0.identifier != "base" }

        var body: some View {
            Group {
                self
                    .previewLayout(PreviewLayout.fixed(width: CGFloat(width), height: CGFloat(height)))
                    .padding()
                    .previewDisplayName("Default preview 1")

                self
                    .previewLayout(PreviewLayout.fixed(width: CGFloat(width), height: CGFloat(height)))
                    .padding()
                    .background(Color(.systemBackground))
                    .environment(\.colorScheme, .dark)
                    .previewDisplayName("Dark Mode")

//                ForEach(localizations, id: \.identifier) { locale in
//                    self
//                        .previewLayout(PreviewLayout.sizeThatFits)
//                        .padding()
//                        .environment(\.locale, locale)
//                        .previewDisplayName(Locale.current.localizedString(forIdentifier: locale.identifier))
//                }
//
//                ForEach(dynamicTypeSizes, id: \.self) { sizeCategory in
//                    self
//                        .previewLayout(PreviewLayout.sizeThatFits)
//                        .padding()
//                        .environment(\.sizeCategory, sizeCategory)
//                        .previewDisplayName("\(sizeCategory)")
//                }

            }
        }
        
        return body
    }
}
