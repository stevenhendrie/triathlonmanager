//
//  Button+Extensions.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 16/02/2021.
//

import SwiftUI

struct OutlineButton: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration
            .label
            .frame(maxWidth: .infinity, minHeight: 40)
            .background(configuration.isPressed ? Color.white : Color.accentColor)
            .foregroundColor(configuration.isPressed ? Color.accentColor : Color.white)
//            .clipShape(RoundedRectangle(cornerRadius: 8))
//            .border(Color.accentColor, width: 2)
    }
}
