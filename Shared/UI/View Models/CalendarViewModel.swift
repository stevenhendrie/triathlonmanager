//
//  CalendarViewModel.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 18/02/2021.
//

import SwiftUI

class CalendarViewModel: ObservableObject {
    
    @Published var date: Date = Date()
    
}
