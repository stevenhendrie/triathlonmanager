//
//  AthleteCreationModel.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 14/02/2021.
//

import Foundation

struct AthleteCreationModel {
    var firstName: String = ""
    var secondName: String = ""
    var dateOfBirth: Date = Date()
    var nationality: Nationality = Nationality.preview
    
    var swimPointsAttributed: Int = 0
    var bikePointsAttributed: Int = 0
    var runPointsAttributed: Int = 0
    
    var isValid: Bool {
        !firstName.isEmpty || !secondName.isEmpty
    }
}
