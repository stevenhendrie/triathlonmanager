//
//  LoadingView.swift
//  TriAppSwiftUI (iOS)
//
//  Created by Steven Hendrie on 28/02/2021.
//

import SwiftUI
import Combine
struct LoadingView: View {
    
    @Environment(\.injected) private var dependencies: DependencyInjection
    @EnvironmentObject private var appState: AppState
    
    var body: some View {
        ProgressView("Loading")
            .onAppear() {
                let _ = dependencies.interactors.staticDataInteractor.loadAllData()
                    .sink (
                        receiveCompletion: { completion in
                            switch completion {
                            case .failure(let error):
                                print(error)
                            case .finished:
                                print("Success")
                            }
                        },
                        receiveValue: { value in
                            appState.routing.app.state = .game
                        })
            }
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
