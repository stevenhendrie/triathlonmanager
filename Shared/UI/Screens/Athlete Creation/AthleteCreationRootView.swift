//
//  AthleteCreationRootView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 14/02/2021.
//

import SwiftUI

struct AthleteCreationRootView: View {
    var body: some View {
        NavigationView {
            AthleteCreationView(pointsToDistribute: 20)
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

extension AthleteCreationRootView {
    struct Routing {
        enum State {
            case creation
            case summary
        }
        var state: State? = .creation
    }
}

struct AthleteCreationRootView_Previews: PreviewProvider {
    static var previews: some View {
        AthleteCreationRootView()
    }
}
