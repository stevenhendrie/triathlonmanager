//
//  AthleteCreationSummaryView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import SwiftUI

struct AthleteCreationSummaryView: View {
    
    @Environment(\.injected) private var dependencies: DependencyInjection
    @EnvironmentObject private var appState: AppState
    
    let athleteInfo: AthleteCreationModel
    
    var body: some View {
        VStack {
            AthleteAttributeRow(title: "First Name", value: athleteInfo.firstName)
            AthleteAttributeRow(title: "Second Name", value: athleteInfo.secondName)
            AthleteAttributeRow(title: "Birth Place", value: athleteInfo.nationality.country)
            AthleteAttributeRow(title: "Swim Level", value: "\(athleteInfo.swimPointsAttributed)")
            AthleteAttributeRow(title: "Bike Level", value: "\(athleteInfo.bikePointsAttributed)")
            AthleteAttributeRow(title: "Run Level", value: "\(athleteInfo.runPointsAttributed)")
            Spacer()
            
            Button("Confirm") {
                appState.routing.app.state = .game
            }
        }
        .navigationTitle("Athlete Summary")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct AthleteCreationSummaryView_Previews: PreviewProvider {
    static var previews: some View {
        AthleteCreationSummaryView(athleteInfo: AthleteCreationModel())
    }
}

struct AthleteAttributeRow: View {
    let title: String
    
    let value: String
    var body: some View {
        HStack{
            Text(title)
            Spacer()
            Text(value)
        }.padding()
    }
}
