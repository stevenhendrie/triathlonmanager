//
//  AthleteCreationView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import SwiftUI

struct AthleteCreationView: View {

    @Environment(\.injected) private var dependencies: DependencyInjection
    @EnvironmentObject private var appState: AppState
    
    @State private var athleteCreationInfo = AthleteCreationModel()
    
    @State private var selectedNationality = 0
    let pointsToDistribute: Int
    
    private var pointsRemainingToDistribute: Int {
        return pointsToDistribute - athleteCreationInfo.swimPointsAttributed - athleteCreationInfo.bikePointsAttributed - athleteCreationInfo.runPointsAttributed
    }
    
    var body: some View {
        VStack {
            Form {
                Section(header: Text("Athlete Name")) {
                    TextField("First Name", text: $athleteCreationInfo.firstName, onCommit:  {
                        UIApplication.shared.endEditing()
                    })
                    TextField("Second Name", text: $athleteCreationInfo.secondName, onCommit:  {
                        UIApplication.shared.endEditing()
                    })
                }
                
                Section(header: Text("Athlete Info")) {
                    DatePicker("Date Of Birth", selection: $athleteCreationInfo.dateOfBirth, in: ...Date(), displayedComponents: .date)
                    
                    Picker(selection: $athleteCreationInfo.nationality, label: Text("Nationality")) {
                        ForEach(appState.staticData.nationalities, id: \.self) {
                            Text($0.country)
                        }
                    }.navigationBarTitle("Nationality", displayMode: .inline)
                }
                
                Section(header: Text("Points")) {
                    HStack {
                        Text("Points Remaining")
                        Spacer()
                        Text("\(pointsRemainingToDistribute)")
                    }
                    PointSlider(title: "Swim", increaseDisabled: pointsRemainingToDistribute == 0, maxValue: 100, value: $athleteCreationInfo.swimPointsAttributed)
                    PointSlider(title: "Bike", increaseDisabled: pointsRemainingToDistribute == 0, maxValue: 100, value: $athleteCreationInfo.bikePointsAttributed)
                    PointSlider(title: "Run", increaseDisabled: pointsRemainingToDistribute == 0, maxValue: 100, value: $athleteCreationInfo.runPointsAttributed)
                }

            }
            
            Spacer()
            
            NavigationLink(destination: AthleteCreationSummaryView(athleteInfo: athleteCreationInfo),
                           tag: AthleteCreationRootView.Routing.State.summary,
                           selection: $appState.routing.athleteCreation.state) { EmptyView() }
            Button(action: { appState.routing.athleteCreation.state = .summary },
                   label: { Text("Next") })
                .disabled(!athleteCreationInfo.isValid || pointsRemainingToDistribute > 0)
        }
        .navigationBarTitle("Athlete Creator", displayMode: .large)
    }
}

struct AthleteNamingView_Previews: PreviewProvider {
    static var previews: some View {
        AthleteCreationView(pointsToDistribute: 5)
            .inject(.preview)
    }
}

