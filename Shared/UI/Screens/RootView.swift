//
//  RootView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import SwiftUI

struct RootView: View {
    
    @Environment(\.injected) private var dependencies: DependencyInjection
    @EnvironmentObject private var appState: AppState
    
    var body: some View {
        switch(appState.routing.app.state) {
        case .welcome:
            WelcomeView()
        case .loading:
            LoadingView()
        case .create:
            AthleteCreationRootView()
                .transition(.move(edge: .trailing))
        case .game:
            GameView()
        case .event:
            EventRootView()
                .transition(.move(edge: .trailing))
        default:
            EmptyView()
        }
    }
}

extension RootView {
    struct Routing {
           
        enum State: String {
            case welcome
            case loading
            case create
            case game
            case event
        }
        var state: State? = .welcome
    }
}

struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
            .inject(.preview)
    }
}
