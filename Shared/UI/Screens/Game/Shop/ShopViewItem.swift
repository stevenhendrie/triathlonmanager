//
//  ShopViewItem.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 15/02/2021.
//

import SwiftUI

struct ShopViewItem: View {

    let item: Item
    
    var body: some View {
        ZStack {
            VStack {
                Text(item.name)
                if let price = NumberFormatter.currency.string(from: NSNumber(value: item.price)) {
                    Text("\(price)")
                }
            }
        }
    }
}

struct ShopViewItem_Previews: PreviewProvider {
        
    static var previews: some View {
        ShopViewItem(item: Item.preview)
    }
}
