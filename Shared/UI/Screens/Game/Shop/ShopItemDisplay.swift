//
//  ShopItemDisplay.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 15/02/2021.
//

import SwiftUI

struct ShopItemDisplay: View {
    @Environment(\.presentationMode) private var presentationMode
    
    @Environment(\.injected) private var dependencies: DependencyInjection
    @EnvironmentObject private var appState: AppState
 
    @State var item: Item
    @State private var canAffordItem: Bool = false
    
    var body: some View {
        VStack {
            Text(item.name)
            if let price = NumberFormatter.currency.string(from: NSNumber(value: item.price)) {
                Text("\(price)")
            }
            Spacer()
            
            Button("Buy") {
                dependencies.interactors.itemInteractor.buyItem(item)
                presentationMode.wrappedValue.dismiss()
            }
            .padding()
            .disabled(!canAffordItem)
        }
        .onAppear {
            dependencies.interactors.itemInteractor.canAffordItem(item, bind: $canAffordItem)
        }
    }
}

struct ShopItemDisplay_Previews: PreviewProvider {
    static var previews: some View {
        ShopItemDisplay(item: Item.preview)
    }
}
