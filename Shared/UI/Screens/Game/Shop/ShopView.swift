//
//  ShopView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import SwiftUI

struct ShopView: View {
    
    @Environment(\.injected) private var dependencies: DependencyInjection
    @EnvironmentObject private var appState: AppState
 
    @State private var items = [Item]()
    @State private var category: Item.Category = .frame
    
    private let columns: [GridItem] =  Array(repeating: .init(.flexible()), count: 4)
 
    @State private var selectedItem: Item!
    
    var body: some View {
  
        let presentItem = Binding<Bool>(
            get: { self.selectedItem != nil },
            set: { self.selectedItem = nil; print("Present Item - \($0)") }
        )
        
        NavigationView {
            VStack{
                Text("Shop")
                    .font(.title)
                HStack {
                    Spacer()
                    Text("Available Funds: \(NumberFormatter.currency.string(for: appState.game.user.money) ?? "0")")
                }
                HStack {
                    Picker("Category", selection: $category) {
                        ForEach(Item.Category.allCases, id: \.self) {
                            Text($0.rawValue.capitalized)
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    .onChange(of: category, perform: { category in
                        reloadItems()
                    })
                }
                ScrollView {
                    LazyVGrid(columns: columns) {
                        ForEach(items) { item in
                            Button(action: {
                                selectedItem = item
                            }, label: {
                                ShopViewItem(item: item)
                                    .frame(minHeight: 100)
                            }).sheet(isPresented: presentItem) {
                                ShopItemDisplay(item: selectedItem!)
                            }
                        }
                    }
                }
            }
            .navigationBarHidden(true)
        }
            .navigationViewStyle(StackNavigationViewStyle())
        .onAppear(perform: reloadItems)
        
    }
    
    private func reloadItems() {
        dependencies.interactors.itemInteractor.items($items, category: self.category)
    }
}

struct ShopView_Previews: PreviewProvider {
    
    static var previews: some View {
        ShopView().inject(.preview)
    }
}
