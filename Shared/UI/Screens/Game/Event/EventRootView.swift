//
//  EventRootView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 16/02/2021.
//

import SwiftUI

struct EventRootView: View {
    
    @Environment(\.injected) private var dependencies: DependencyInjection
    @EnvironmentObject private var appState: AppState
    
    var body: some View {
        VStack {
            switch(appState.routing.event.state) {
            case .prerace:
                PreRaceView()
            case .race:
                RaceView()
            case .postrace:
                PostRaceView()
            }
        }
        .environmentObject(appState.game.currentEvent)
    }
    
}

extension EventRootView {
    struct Routing {
         
        enum State {
            case prerace
            case race
            case postrace
        }
        var state: State = .prerace
    }
}

struct EventRootView_Previews: PreviewProvider {
    
    static var previews: some View {
        EventRootView()
    }
}

