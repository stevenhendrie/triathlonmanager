//
//  EventDayView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 17/02/2021.
//

import SwiftUI

struct RaceView: View {
    
    @Environment(\.injected) private var dependencies: DependencyInjection
    
    @EnvironmentObject private var appState: AppState
    @EnvironmentObject private var event: Event
    
    @State private var startButtonDisplayed = true
    @State private var raceComplete = false
    
    var body: some View {
        VStack {
            HStack(alignment: .top) {
                VStack {
                    RaceMapView()
                    
                }
                VStack {
                    Text("Race Time - \(event.elapsedTime.timeString)")
                    RaceLeaderboardView()
                }
            }
            
            if event.state == .raceDay {
                Button(action: {
                    withAnimation(.easeOut(duration: 0.3)) {
                        dependencies.interactors.raceInteractor.startRace(event: event)
                    }
                }, label: {
                    Text("Start")
                })
            }
            
            if event.state == .complete {
                Button(action: {
                    withAnimation(.easeOut(duration: 0.3)) {
                        appState.routing.event.state = .postrace
                    }
                }, label: { Text("Complete Race") })
            }
        }
    }
}


struct RaceView_Previews: PreviewProvider {
    
    static var previews: some View {
        RaceView()
            .environmentObject(Event.preview)
            .fixedSize()
            .uiPreview(width: 800, height: 800)
    }
}
