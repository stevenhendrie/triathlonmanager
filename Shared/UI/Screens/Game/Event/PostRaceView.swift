//
//  EventCompleteView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 16/02/2021.
//

import SwiftUI


struct PostRaceView: View {
    
    @EnvironmentObject private var appState: AppState
    @EnvironmentObject private var event: Event
    
    var body: some View {
        VStack {
            RaceLeaderboardView()
            
            Button(action: {
                withAnimation(.easeOut(duration: 0.3)) {
                    appState.routing.app.state = .game
                }
            }, label: { Text("Complete Race") })
        }
    }
}

struct PostRaceView_Previews: PreviewProvider {
    
    static var previews: some View {
        PostRaceView()
            .environmentObject(Event.preview)
            .uiPreview(width: 200, height: 100)
    }
}
