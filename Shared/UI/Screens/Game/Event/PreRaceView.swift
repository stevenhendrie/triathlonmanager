//
//  EventView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 15/02/2021.
//

import SwiftUI

struct PreRaceView: View {
    
    @Environment(\.injected) private var dependencies: DependencyInjection
    @EnvironmentObject private var appState: AppState
    @EnvironmentObject private var event: Event
    
    var body: some View {
        VStack {
            HStack(spacing: 5) {
                RaceInfoView()
                    .padding()
              
                AthleteListing(athletes: event.athletes) { athlete in
                        HStack {
                            Text(athlete.fullName)
                        }
                }
                .frame(maxWidth: 400, maxHeight: 400)
            }
            Spacer()
            Button(action: {
                withAnimation(.easeInOut(duration: 0.5)) {
                    appState.routing.event.state = .race
                }
            }, label: {
                Text("Complete Race")
            })

        }
    }
}

struct EventView_Previews: PreviewProvider {
    
    struct PreviewContainer : View {
        @State private var event = Event.preview
        
         var body: some View {
            PreRaceView()
                .environmentObject(Event.preview)
         }
    }
    
    static var previews: some View {
        PreviewContainer().uiPreview(width: 200, height: 100)
    }
}

