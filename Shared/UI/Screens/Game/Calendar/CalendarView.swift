//
//  CalendarView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 15/02/2021.
//

import SwiftUI

struct CalendarView: View {
    
    @Environment(\.calendar) var calendar
    @Environment(\.injected) private var dependencies: DependencyInjection
    
    var viewModel = CalendarViewModel()
    
    init() {
        viewModel.date = dependencies.appState.game.currentDay.date
    }
    
    var body: some View {
        NavigationView {
            VStack {
               CalendarDateMoverView()
               CalendarMonthView()
            }
            .navigationBarHidden(true)
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .environmentObject(viewModel)
    }
}

struct CalendarView_Previews: PreviewProvider {
    static var previews: some View {
        CalendarView()
    }
}
