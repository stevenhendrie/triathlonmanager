//
//  HomeView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 14/02/2021.
//

import SwiftUI

struct HomeView: View {
    @Environment(\.calendar) var calendar
    @Environment(\.injected) private var dependencies: DependencyInjection
        
    @EnvironmentObject private var appState: AppState
        
    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    // Today Summary View
                    TodaySummaryView(
                        date: $appState.game.currentDay.date
                    )
                    .frame(maxWidth: 300, maxHeight: 300)
                    .border(Color.black, width: 2)
                    .padding()
                    
                    Spacer()
                    // Training Summary View
                }
                HStack {
                    // News View
                    // League/Rankings View
                }
                Spacer()
            }
            .navigationBarTitle("Day", displayMode: .inline)
            .navigationBarItems(trailing:
                                    HStack {
                                        Button(action: {
                                            
                                            dependencies.interactors.timelineInteractor.moveToNextDay()
                                        }) {
                                            Image(systemName: "plus.square.fill")
                                                .font(.largeTitle)
                                        }
                                        .foregroundColor(.blue)
                                    })
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .onAppear() {
            dependencies.interactors.eventsInteractor.setCurrentDayEvents()
        }
    }
}


struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            HomeView()
                .inject(.preview)
        }
    }
}
