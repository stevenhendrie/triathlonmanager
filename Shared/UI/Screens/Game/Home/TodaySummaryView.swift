//
//  TodaySummaryView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 14/02/2021.
//

import SwiftUI

struct TodaySummaryView: View {
    
    @Environment(\.injected) private var dependencies: DependencyInjection
    @EnvironmentObject private var appState: AppState
    @Binding var date: Date

    
    private func startEvent(_ event: Event) {
        dependencies.interactors.eventsInteractor.startEvent(event)
    }
    
    var body: some View {
        VStack {
            Text(DateFormatter.todayViewDate.string(from: date))
            List {
                ForEach(appState.game.currentDay.events) { event in
                    
                    if event.state != .complete {
                        Button(action: {
                            withAnimation(.easeInOut(duration: 0.5)) {
                                startEvent(event)
                            }
                        }, label: {
                            Text("Start \(event.name)")
                        })
                    } else {
                        Text("\(event.name) complete" )
                    }
                }
            }
        }
        .padding()
//        .onAppear() {
//            updateEvents()
//        }
//        .onChange(of: date, perform: { value in
//            updateEvents()
//        })
    }
}

struct TodaySummaryView_Previews: PreviewProvider {
    
    struct PreviewContainer : View {
         @State private var date = Date()
         var body: some View {
            TodaySummaryView(date: $date)
         }
    }
    
    static var previews: some View {
        PreviewContainer()
    }
}
