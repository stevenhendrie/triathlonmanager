//
//  GameView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import SwiftUI

struct GameView: View {
    
    @Environment(\.injected) private var dependencies: DependencyInjection
    @EnvironmentObject private var appState: AppState
    
    var body: some View {
        
        TabView {
            HomeView().tabItem { Image(systemName: "house.fill"); Text("Home") }
            CalendarView().tabItem { Image(systemName: "calendar"); Text("Calendar") }
            ShopView().inject(dependencies).tabItem { Image(systemName: "bag.fill"); Text("Shop") }
        }
    }
}

extension GameView {
    struct Routing {
        
    }
}

struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            GameView()
        }
    }
}
