//
//  WelcomeView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import SwiftUI

struct WelcomeView: View {
    
    @Environment(\.injected) private var dependencies: DependencyInjection
    @EnvironmentObject private var appState: AppState
    
    var body: some View {
        VStack {
            Text("Total Triathlon Training")
                .font(.title)
            
            Spacer()
        
            Button("Continue") {
                appState.routing.app.state = .loading
            }
            
            Button("New Game") {
                withAnimation(.easeInOut(duration: 0.5)) {
                    appState.routing.app.state = .create
                }
            }
            
        }
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
            .uiPreview(width: 500, height: 500)
    }
}
