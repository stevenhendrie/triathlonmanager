//
//  RaceUpdateSpeedView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 18/02/2021.
//

import SwiftUI

struct RaceUpdateSpeedView: View {
    
    @Binding var raceSpeed: Double
    
    var body: some View {
        VStack {
            Slider(value: $raceSpeed, in: 1...5, step: 1)
                .accentColor(.green)
        }
    }
}

struct RaceUpdateSpeedView_Previews: PreviewProvider {
    
    struct PreviewContainer : View {
        @State private var raceSpeed = 2.0
        
         var body: some View {
            RaceUpdateSpeedView(raceSpeed: $raceSpeed)
         }
    }
    
    static var previews: some View {
        PreviewContainer().uiPreview(width: 200, height: 100)
    }
}

