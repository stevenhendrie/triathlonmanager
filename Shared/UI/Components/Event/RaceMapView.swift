//
//  RaceMapView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 18/02/2021.
//

import SwiftUI

struct RaceMapView: View {
    
    @EnvironmentObject private var event: Event
    
    var body: some View {
        VStack {
            Text("MAP")
            HStack (alignment: .top) {
                VStack {
                    //MAP VIEW
                    VStack {
                        Text("MAP")
                    }
                    //ELEVATION VIEW
                    VStack {
                        Text("ELEVATION")
                        
                        GeometryReader { metrics in
                            ZStack(alignment: .topLeading) {
                                
                                
                                Path { path in
                                    path.move(to: CGPoint(x: 0, y: 0))
                                    path.addLine(to: CGPoint(x: metrics.size.width, y: 0))
                                }.stroke(Color.blue, lineWidth: 2)
                                
                                ForEach(event.athletes) { athlete in
                                    
                                    if let progress = event.results[athlete.id], progress.state == .started {
                                    
                                        let percentage = (progress.distanceComplete / event.totalDistance)
                                        Circle()
                                            .fill(Color.red)
                                            .position(x: metrics.size.width * CGFloat(percentage), y: 0)
                                            .frame(width: 5, height: 5)
                                    }
                                }
                            }
                        }
                    }
                }
                //EVENT INFO VIEW
                VStack {
                    Text("Event Info")
                }
            }
        }
    }
}

struct RaceMapView_Previews: PreviewProvider {
    
    static var previews: some View {
        RaceMapView()
            .environmentObject(Event.preview)
    }
}

