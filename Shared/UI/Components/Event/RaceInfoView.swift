//
//  RaceInfoView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 18/02/2021.
//

import SwiftUI

struct RaceInfoView: View {
           
    @EnvironmentObject private var event: Event
    
    var body: some View {
        VStack {
            HStack {
                VStack(alignment: .trailing, spacing: 10) {
                    Text("Event - ")
                    Text("Prize - ")
                    Text("Location - ")
                    Text("Type - ")
                }
                VStack(alignment: .leading, spacing: 10) {
                    Text("\(event.name)")
                    Text("\(NumberFormatter.currency.string(from: NSNumber(value: event.prizeMoney)) ?? "0")")
                    Text("\(event.location.country)")
                    Text("\(event.type.rawValue.capitalized) (\(event.category.rawValue.capitalized))")
                }
            }
            HStack {
                ForEach(event.format, id: \.self) { section in
                    VStack {
                        Text("\(section.discipline.rawValue.capitalized)")
                        Text("\(section.distance.distanceString)")
                    }
                    .padding()
                }
            }
        }
        .padding()
        .border(Color.black, width: 2)
    }
}

struct RaceInfoView_Previews: PreviewProvider {
    
    
    static var previews: some View {
       RaceInfoView()
            .environmentObject(Event.preview)
            .uiPreview(width: 200, height: 100)
    }
}
