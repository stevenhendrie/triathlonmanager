//
//  AthleteListing.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 17/02/2021.
//

import SwiftUI

struct AthleteListing<Content: View>: View {
    
    typealias AthleteSortMethod = ((Athlete, Athlete) -> Bool)
    
    @State private(set) var selectedGender: Athlete.Gender = .male
    @State private(set) var selectedAgeGroup: Athlete.AgeGroup = .all
    
    let content: (Athlete) -> Content
    let athletes: [Athlete]
    let sortMethod: AthleteSortMethod
    
    init(athletes: [Athlete], sortBy: @escaping AthleteSortMethod = { _,_ in return true }, @ViewBuilder _ content: @escaping (Athlete) -> Content) {
        self.athletes = athletes
        self.sortMethod = sortBy
        self.content = content
    }
    
    var body: some View {
        VStack {
            Picker(selection: $selectedGender, label: Text("Gender")) {
                ForEach(Athlete.Gender.allCases, id: \.self) { gender in
                    Text("\(gender.rawValue.capitalized)")
                }
            }
            .pickerStyle(SegmentedPickerStyle())
                
            Picker(selection: $selectedAgeGroup, label: Text("Age Group - \(selectedAgeGroup.rawValue)")) {
                    ForEach(Athlete.AgeGroup.allCases, id: \.self) { ageGroup in
                        Text("\(ageGroup.rawValue.capitalized)")
                    }
                }
                .pickerStyle(MenuPickerStyle())
            
            List {
                let filtered = athletes.filter({
                    let equalGender = $0.gender == selectedGender
                    if selectedAgeGroup == .all {
                        return equalGender
                    } else {
                        return equalGender && $0.ageGroup == selectedAgeGroup
                    }
                })
                let sorted = filtered.sorted(by: sortMethod)
                ForEach(sorted) { athlete in
                    content(athlete)
                }
            }
            .animation(.linear)
        }
    }
}

struct AthleteListing_Previews: PreviewProvider {
    static var previews: some View {
        AthleteListing(athletes: [Athlete.preview], { athletes in
            Text("Test")
        }).uiPreview(width: 300, height: 400)
    }
}
