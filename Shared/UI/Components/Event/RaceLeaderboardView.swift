//
//  RaceLeaderboardView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 18/02/2021.
//

import SwiftUI

struct RaceLeaderboardView: View {
        
    @EnvironmentObject private var event: Event
    
    var body: some View {
        VStack {
            AthleteListing(athletes: event.athletes, sortBy: {
                if let racer1Stats = event.results[$0.id],
                   let racer2Stats = event.results[$1.id] {
                    if racer1Stats.distanceComplete != racer2Stats.distanceComplete {
                        return racer1Stats.distanceComplete > racer2Stats.distanceComplete
                    } else {
                        return racer1Stats.time < racer2Stats.time
                    }
                } else {
                    return true
                }
            }) { athlete in
                
                HStack {
                    
                    Text(athlete.fullName)
                    
                    if let result = event.results[athlete.id] {
                    
                        switch(result.state) {
                        case .didNotStart:
                            Text("DNS")
                        case .didNotFinish:
                            Text("DNF")
                            if let distance = result.distanceComplete.distanceString {
                                Text(distance)
                            }
                        case .started:
                            if let distance = result.distanceComplete.distanceString {
                                Text(distance)
                            }
                        case .completed:
                            if let time = result.time.timeString {
                                Text(time)
                            }
                        default:
                            EmptyView()
                        }
                       
                    }
                }
                
                
            }
        }
    }
}

struct RaceLeaderboardView_Previews: PreviewProvider {
    
    static var previews: some View {
        RaceLeaderboardView()
            .environmentObject(Event.preview)
            .uiPreview(width: 200, height: 400)
    }
}

