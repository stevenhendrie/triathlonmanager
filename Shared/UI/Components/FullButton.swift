//
//  FullButton.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import SwiftUI

struct FullButton: View {
    var text: String
    var action: () -> ()
    var body: some View {
        
        Button(self.text) {
            action()
        }
        .frame(height: 50)
        .frame(maxWidth: .infinity)
        .background(Color.blue)
        .clipShape(Capsule())
        .foregroundColor(Color.white)
        .buttonStyle(PlainButtonStyle())
        .padding([.top, .bottom], 5)
        .padding([.leading, .trailing], 10)
    }
}

struct FullButton_Previews: PreviewProvider {
    static var previews: some View {
        FullButton(text: "Hello ") {
            
        }
    }
}
