//
//  CalendarView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 14/02/2021.
//

import SwiftUI

struct CalendarMonthView: View {
    @Environment(\.calendar) var calendar
    
    @EnvironmentObject var viewModel: CalendarViewModel
    
    var body: some View {
        VStack(spacing: 0) {
            ForEach(viewModel.date.weeksOfMonth, id: \.self) { date in
                CalendarWeekView(week: date)
            }
        }
    }
}

struct CalendarMonthView_Previews: PreviewProvider {
    static var previews: some View {
        CalendarMonthView()
            .environmentObject(CalendarViewModel())
    }
}
