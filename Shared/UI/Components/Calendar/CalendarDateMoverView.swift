//
//  CalendarDateMoverView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 15/02/2021.
//

import SwiftUI

struct CalendarDateMoverView: View {
    @Environment(\.calendar) var calendar

    @EnvironmentObject var viewModel: CalendarViewModel
    
    var body: some View {
        HStack {
            Button("Previous Month") {
                if let date = calendar.date(byAdding: .month, value: -1, to: viewModel.date) {
                    viewModel.date = date
                }
            }
            Text(DateFormatter.monthYear.string(from: viewModel.date))
            Button("Next Month") {
                if let date = calendar.date(byAdding: .month, value: 1, to: viewModel.date) {
                    viewModel.date = date
                }
            }
        }
    }
}

struct HomeDateMover_Previews: PreviewProvider {
        
    static var previews: some View {
        CalendarDateMoverView()
            .environmentObject(CalendarViewModel())
    }
}
