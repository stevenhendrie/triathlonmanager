//
//  CalendarWeekView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 15/02/2021.
//

import SwiftUI

struct CalendarWeekView: View {
    @Environment(\.calendar) var calendar
    
    var week: Date
        
    var body: some View {
        HStack(spacing: 0) {
            ForEach(week.daysOfWeek, id: \.self) { day in
                CalendarDayView(day: day)
                    .isHidden(!calendar.isDate(self.week, equalTo: day, toGranularity: .month))
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
            }
        }
    }
}

struct CalendarWeekView_Previews: PreviewProvider {
    static var previews: some View {
        CalendarWeekView(week: Date())
    }
}
