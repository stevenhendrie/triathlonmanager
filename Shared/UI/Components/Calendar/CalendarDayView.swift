//
//  CalendarDayView.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 15/02/2021.
//

import SwiftUI

struct CalendarDayView: View {
    
    @Environment(\.calendar) var calendar
    @Environment(\.injected) private var dependencies: DependencyInjection
    @EnvironmentObject private var appState: AppState
    var day: Date
    
    @State var events: [Event] = []
    @State var isToday: Bool = false
    
    init(day: Date) {
        self.day = day
    }
    
    var body: some View {
        VStack {
            Text(DateFormatter.shortWeekday.string(from: day))
                .padding(5)
                .frame(maxWidth: .infinity)
            List{
                ForEach(events) { event in
                    
                    if event.state == .complete {
                        Button(action: {
                            appState.routing.event.state = .postrace
                            appState.routing.app.state = .event
                        }, label: { Text("\(event.name)") })
                        
                    } else {
                        Text("\(event.name)")
                    }
                }
                .listStyle(DefaultListStyle())
            }
            Spacer()
        }.border(isToday ? Color.blue : Color.green, width: 3)
        .onAppear() {
            dependencies.interactors.eventsInteractor.eventsOnDate(day, events: $events)
            dependencies.interactors.timelineInteractor.isToday(date: day, isToday: $isToday)
        }
    }
}
struct CalendarDayView_Previews: PreviewProvider {
    static var previews: some View {
        let width = 200.0
        let height = 200.0
        Group {
            CalendarDayView(day: Date()).uiPreview(width: width, height: height)
            CalendarDayView(day: Date(timeIntervalSinceNow: 86400)).uiPreview(width: width, height: height)
        }
    }
}
