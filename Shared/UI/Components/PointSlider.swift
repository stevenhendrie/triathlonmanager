//
//  PointSlider.swift
//  TriAppSwiftUI
//
//  Created by Steven Hendrie on 13/02/2021.
//

import SwiftUI

struct PointSlider: View {
    
    var title: String
    var increaseDisabled: Bool
    var maxValue: Int
    
    @Binding var value: Int
    
    var body: some View {
        VStack {
            HStack {
                Text(title)
                    .frame(width: 60)
                Spacer()
                Button("-") {
                    self.value -= 1
                }
                .buttonStyle(PlainButtonStyle())
                .disabled(self.value == 0)
                
                VStack {
                    Text("\(self.value)")
                    ProgressView(value: Double(self.value), total: Double(self.maxValue))
                }
                    
                    .padding()
                Button("+") {
                    self.value += 1
                }
                .buttonStyle(PlainButtonStyle())
                .disabled(self.increaseDisabled)
            }
            
        }
    }
}


struct PointSlider_Previews: PreviewProvider {
    
    @State static var value = 5
    
    static var previews: some View {
        PointSlider(title: "Title", increaseDisabled: false, maxValue: 100, value: $value)
    }
}
